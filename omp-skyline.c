/****************************************************************************
 *
 * skyline.c
 *
 * Serial implementaiton of the skyline operator
 *
 * Copyright (C) 2020 Moreno Marzolla
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * --------------------------------------------------------------------------
 *
 * Questo programma calcola lo skyline di un insieme di punti in D
 * dimensioni letti da standard input. Per una descrizione completa
 * si veda la specifica del progetto sul sito del corso:
 *
 * https://www.moreno.marzolla.name/teaching/HPC/
 *
 * Per compilare:
 *
 * gcc -D_XOPEN_SOURCE=600 -std=c99 -Wall -Wpedantic -O2 skyline.c -o skyline
 *
 * (il flag -D_XOPEN_SOURCE=600 e' superfluo perche' viene settato
 * nell'header "hpc.h", ma definirlo tramite la riga di comando fa si'
 * che il programma compili correttamente anche se non si include
 * "hpc.h", o non lo si includa come primo file).
 *
 * Per eseguire il programma:
 *
 * ./skyline < input > output
 *
 ****************************************************************************/
#define _XOPEN_SOURCE 600
#define SIMD

#include "hpc.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <x86intrin.h>

typedef struct
{
    float *P; /* coordinates P[i][j] of point i               */
    int N;    /* Number of points (rows of matrix P)          */
    int D;    /* Number of dimensions (columns of matrix P)   */
    int Dr;   /* Number of dimensions for the simd alignment(columns of matrix P) */
} points_t;

typedef int v4i __attribute__((vector_size(16)));
typedef float v4f __attribute__((vector_size(16)));
#define VLEN (int)(sizeof(v4f) / sizeof(float))

/**
 * Read input from stdin. Input format is:
 *
 * d [other ignored stuff]
 * N
 * p0,0 p0,1 ... p0,d-1
 * p1,0 p1,1 ... p1,d-1
 * ...
 * pn-1,0 pn-1,1 ... pn-1,d-1
 *
 */
void read_input(points_t *points)
{
    char buf[1024];
    int N, D, i, k;
    float *P;

    if (1 != scanf("%d", &D))
    {
        fprintf(stderr, "FATAL: can not read the dimension\n");
        exit(EXIT_FAILURE);
    }
    assert(D >= 2);
    if (NULL == fgets(buf, sizeof(buf), stdin))
    { /* ignore rest of the line */
        fprintf(stderr, "FATAL: can not read the first line\n");
        exit(EXIT_FAILURE);
    }
    if (1 != scanf("%d", &N))
    {
        fprintf(stderr, "FATAL: can not read the number of points\n");
        exit(EXIT_FAILURE);
    }
    int Dr = D + (VLEN - (D % VLEN));
    long size = Dr * N * sizeof(*P);
    int ret = posix_memalign((void **)&P, __BIGGEST_ALIGNMENT__, size);
    assert(0 == ret);
    for (i = 0; i < N; i++)
    {
        for (k = 0; k < D; k++)
        {
            if (1 != scanf("%f", &(P[i * Dr + k])))
            {
                fprintf(stderr, "FATAL: failed to get coordinate %d of point %d\n", k, i);
                exit(EXIT_FAILURE);
            }
        }
    }
    points->P = P;
    points->N = N;
    points->D = D;
    points->Dr = Dr;
}

void free_points(points_t *points)
{
    free(points->P);
    points->P = NULL;
    points->N = points->D = points->Dr = -1;
}

/* Returns 1 iff |p| dominates |q| */
int dominates_simd(const float *p, const float *q, int D)
{
    int k;
    int i;
    v4f *vp = (v4f *)p;
    v4f *vq = (v4f *)q;
    v4i vm = (v4i)_mm_set1_epi32(0);
    v4i vd = (v4i)_mm_set1_epi32(0);

    /* The following loop could be merged, but the keep them separated
       for the sake of readability */
    for (k = 0; k < D - VLEN + 1; k += VLEN)
    {
        vm = (vm | (*vp < *vq));
        vd = (vd | (*vp > *vq));
        vp++;
        vq++;
    }

    for (i = k; i < D; i++)
    {
        if (p[i] < q[i])
        {
            return 0;
        }
    }
    if (k != 0)
    {
        int m = 0;
        int d = 0;
        for (i = 0; i < VLEN; i++)
        {
            m = m || vm[i];
            d = d || vd[i];
            if (m != 0)
            {
                return 0;
            }
        }
        if (d != 0)
        {
            return 1;
        }
    }
    for (; k < D; k++)
    {
        if (p[k] > q[k])
        {
            return 1;
        }
    }
    return 0;
}

int dominates(const float *p, const float *q, int D)
{
    int k;

    /* The following loop could be merged, but the keep them separated
       for the sake of readability */
    for (k = 0; k < D; k++)
    {
        if (p[k] < q[k])
        {
            return 0;
        }
    }
    for (k = 0; k < D; k++)
    {
        if (p[k] > q[k])
        {
            return 1;
        }
    }
    return 0;
}

/**
 * Compute the skyline of |points|. At the end, s[i] == 1 iff point
 * |i| belongs to the skyline. This function returns the number r of
 * points in to the skyline. The caller is responsible for allocating
 * a suitably sized array |s|.
 */
int skyline(const points_t *points, int *s)
{
    const int D = points->D;
    const int Dr = D + (VLEN - (D % VLEN));
    const int N = points->N;
    const float *P = points->P;
    int i, j, r = N;

    const v4i ones = (v4i)_mm_set1_epi32(1);
    const int size = (N + VLEN - (N % VLEN));

#pragma omp parallel default(none) shared(s, P) private(i) reduction(- \
                                                                     : r)
    {
#pragma omp for schedule(static)
        for (i = 0; i < size; i = i + VLEN)
        {
            v4i *vmy_s = (v4i *)&(s[i]);
            *vmy_s = ones;
        }

        for (i = 0; i < N; i++)
        {
            if (s[i])
            {
#pragma omp for
                for (j = 0; j < N; j++)
                {
#ifdef SIMD
                    if (j != i && s[j] && dominates_simd(&(P[i * Dr]), &(P[j * Dr]), D))
#else
                    if (j != i && s[j] && dominates(&(P[i * Dr]), &(P[j * Dr]), D))
#endif
                    {
                        s[j] = 0;
                        r--;
                    }
                }
            }
        }
    }
    return r;
}

/**
 * Print the coordinates of points belonging to the skyline |s| to
 * standard ouptut. s[i] == 1 iff point i belongs to the skyline.  The
 * output format is the same as the input format, so that this program
 * can process its own output.
 */
void print_skyline(const points_t *points, const int *s, int r)
{
    const int D = points->D;
    const int Dr = points->Dr;
    const int N = points->N;
    const float *P = points->P;

    int i, k;

    printf("%d\n", D);
    printf("%d\n", r);
    for (i = 0; i < N; i++)
    {
        if (s[i])
        {
            for (k = 0; k < D; k++)
            {
                printf("%f ", P[i * Dr + k]);
            }
            printf("\n");
        }
    }
}

int main(int argc, char *argv[])
{
    points_t points;

    if (argc != 1)
    {
        fprintf(stderr, "Usage: %s < input_file > output_file\n", argv[0]);
        return EXIT_FAILURE;
    }

    read_input(&points);
    int *s;
    int size = (points.N + VLEN - (points.N % VLEN));
    int ret = posix_memalign((void **)&s, __BIGGEST_ALIGNMENT__, (long)size * sizeof(*s));
    assert(0 == ret);
    const double tstart = hpc_gettime();
    const int r = skyline(&points, s);
    const double elapsed = hpc_gettime() - tstart;
    print_skyline(&points, s, r);

    fprintf(stderr,
            "\n\t%d points\n\t%d dimensione\n\t%d points in skyline\n\nExecution time %f seconds\n",
            points.N, points.D, r, elapsed);

    free_points(&points);
    free(s);
    return EXIT_SUCCESS;
}
