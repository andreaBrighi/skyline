**Skyline operator implemented with MPI, Open MPI and SIMD instruction**

## Skyline operator

Took a set P = {P0, P2, ... PN - 1} with N point in a D dimensions space; point Pi has coordinates (Pi, 0, Pi, 1. ..., Pi, D - 1).

Pi dominate Pj if both condition are met:

1. For each dimension k = 0, ... D - 1, Pi, k ≥ Pj, k

2. Exist at least one coordiante that  for which the inequality referred to in the previous point holds in strict sense


The skyline Sk(P) is the set of points P which are not dominated by no other points (by definition a point does not dominate himself):

Sk(P) = {s ∈ P : does not exist t in P that t dominates s}

*skyline algorithm*

SKYLINE( P0, P1, ... PN-1 ) 
	
		bool S[0..N - 1];
		integer i, j;
		for i ← 0 to N - 1 do
			S[i] ← true;	
		endfor
		for i ← 0 to N - 1 do
			if S[i] then
				for j ← 0 to N - 1 do
					if (S[j] and Pi domina Pj) then
						S[j] ← false;
					endif 	
				endfor	
			endif	
		endfor
		for i ← 0 to N - 1 do 
			if S[i] then
				print Pi 	
			endif	
		endfor
		
---

## OpenMP version

- use the Makefile to compile the project with: 	make omp
- executethe project with:	./omp-skyline < input_file > output_file

---

## MPI version

- use the Makefile to compile the project with:		make mpi	
- execute the project with: 	mpiexec -n core number mpi-skyline < input_file > output_file

---

## SIMD version

- in the file .c uncomment the define SIMD line
- use the Makefile to compile the project as describide previusly

